package com.minetoblend

import com.minetoblend.gui.MainGui
import com.minetoblend.gui.Window
import com.minetoblend.project.Project

object Program {

    lateinit var mainWindow: Window

    var currentProject: Project? = null

    fun run() {
        init()
    }

    fun init() {
        mainWindow = Window(MainGui())
    }


}