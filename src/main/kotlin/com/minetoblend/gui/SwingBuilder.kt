package com.minetoblend.gui

import java.awt.Color
import java.awt.Component
import java.awt.Container
import java.awt.Label
import java.awt.event.ActionEvent
import javax.swing.*

fun menubar(init: JMenuBar.() -> Unit): JMenuBar {
    var menubar = JMenuBar()
    menubar.init()
    return menubar
}

fun JMenuBar.menu(name: String, init: JMenu.() -> Unit): JMenu {
    var menu = JMenu(name)
    menu.init()
    add(menu)
    return menu
}

fun JMenu.menu(name: String, init: (JMenu.() -> Unit) = {}): JMenu {
    var menu = JMenu(name)
    menu.init()
    add(menu)
    return menu
}

fun JMenu.item(name: String = "", init: (JMenuItem.() -> Unit) = {}): JMenuItem {
    var item = JMenuItem(name)
    item.init()
    add(item)
    return item
}

fun JMenu.separator() {
    addSeparator()
}


fun JMenuItem.accelerator(keys: String) {
    accelerator = KeyStroke.getKeyStroke(keys)
}

fun JMenuItem.mnemonic(c: Char) {
    mnemonic = c.toInt()
}

infix fun JMenuItem.action(listener: (ActionEvent) -> Unit) {
    addActionListener { e -> listener(e) }
}

fun JMenu.items(len: Int, init: JMenuItem.(i: Int) -> Unit) {
    for (i in 0..len - 1) {
        var item = JMenuItem()
        item.init(i)
        add(item)
    }
}

fun panel(init: JPanel.() -> Unit): JPanel {
    var panel = JPanel()
    panel.init()
    return panel
}

fun Container.button(text: String = "", layoutArgs: String? = null, init: (JButton.() -> Unit) = {}): JButton {
    var button = JButton(text)
    button.init()
    if (layoutArgs == null)
        add(button)
    else
        add(button, layoutArgs)
    return button
}

infix fun JButton.action(listener: (ActionEvent) -> Unit) {
    addActionListener { e -> listener(e) }
}

fun Container.panel(layoutArgs: String? = null, init: JPanel.() -> Unit): JPanel {
    var panel = JPanel()
    panel.init()
    if (layoutArgs == null)
        add(panel)
    else
        add(panel, layoutArgs)
    return panel
}

fun Container.label(text: String = "", init: JLabel.() -> Unit) : JLabel{
    var label = JLabel(text)

    return label
}