package com.minetoblend.gui

import com.minetoblend.Program
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dimension
import java.awt.FlowLayout


class MainGui : Gui {

    constructor() {
        initRootComponent()
        initMenu()
    }

    private fun initRootComponent() {
        rootComponent = panel {
            layout = BorderLayout()
            background = Color(0xffffff)
            panel(BorderLayout.NORTH) {
                background = Color(0xcccccc)
                preferredSize = Dimension(24, 24)
            }
            panel(BorderLayout.WEST) {
                // Toolbar
                preferredSize = Dimension(200, 100)
                background = Color(0xeeeeee)
            }
            panel(BorderLayout.EAST) {
                // Settings bar
                preferredSize = Dimension(200, 100)
                background = Color(0xeeeeee)
            }
            panel(BorderLayout.SOUTH) {
                // Status bar
                layout = FlowLayout()
                background = Color(0xcccccc)
                label {
                    Program.status.subscribe
                }
            }

        }
    }


    private fun initMenu() {

        menuBar = menubar {
            menu("file") {
                item("new") {
                    accelerator("ctrl N")
                    mnemonic('n')
                    action { println("you clicked new") }
                }
                item("open") {
                    accelerator("ctrl O")
                    mnemonic('o')
                }
                menu("open recent") {
                    mnemonic('c')
                    items(3) { i ->
                        text = "Menu item $i"
                        mnemonic(i.toString()[0])
                    }
                }
                separator()

                item("save") {
                    accelerator("ctrl S")
                    mnemonic('a')
                }
                item("save as") {
                    accelerator("ctrl shift S")
                    mnemonic('a')
                }
            }

            menu("edit") {
                item("undo")
                item("redo")
            }
        }
    }


}