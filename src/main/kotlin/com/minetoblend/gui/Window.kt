package com.minetoblend.gui

import java.awt.Dimension
import javax.swing.JComponent
import javax.swing.JFrame
import javax.swing.WindowConstants

class Window : JFrame {

    constructor(gui: Gui) {
        add(gui.rootComponent)
        jMenuBar = gui.menuBar
        defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
        size = Dimension(800, 600)
        setLocationRelativeTo(null)
        isVisible = true
    }


}